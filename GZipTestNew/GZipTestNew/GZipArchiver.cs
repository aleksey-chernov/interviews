﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Threading;

namespace GZipTestNew
{
    public class GZipArchiver : IDisposable
    {
        #region Fields

        private const int BufferSize = 64 * 1024;

        private readonly Stream _inStream, _outStream;
        
        private Thread[] _transformThreads = new Thread[Environment.ProcessorCount];
        private Thread _outputThread;

        private readonly FixedBlockedQueue<Chunk> _transformQueue = new FixedBlockedQueue<Chunk>(100);
        private readonly FixedBlockedQueue<Chunk> _outputQueue = new FixedBlockedQueue<Chunk>(100);

        private readonly EventWaitHandle _startEvent = new ManualResetEvent(false);
         
        #endregion

        #region Constructors

        public GZipArchiver(Stream inStream, Stream outStream)
        {
            if (inStream == null)
                throw new ArgumentNullException("inStream");
            if (outStream == null)
                throw new ArgumentNullException("outStream");

            _inStream = inStream;
            _outStream = outStream;
        }

        #endregion

        #region Public Methods

        public void Compress()
        {
            InitTransformThreads(CompressThread);
            _outputThread = new Thread(PasteThread);
            _outputThread.Start("compression");

            _startEvent.Set();

            int chunkId = 0;
            int readed;

            while (true)
            {
                var buffer = new byte[BufferSize];
    
                readed = _inStream.Read(buffer, 0, buffer.Length);

                if (readed == 0)
                {
                    _transformQueue.Enqueue(null);
                    _outputQueue.Enqueue(null);
                    break;
                }

                var chunk = new Chunk(chunkId++, buffer, readed);

                if (!_transformQueue.TryEnqueue(chunk) || !_outputQueue.TryEnqueue(chunk))
                    break;
            }

            foreach (Thread thread in _transformThreads)
            {
                thread.Join();
            }
            _outputThread.Join();
        }

        public void Decompress()
        {
            InitTransformThreads(DecompressThread);
            _outputThread = new Thread(PasteThread);
            _outputThread.Start("decompression");

            _startEvent.Set();

            int chunkId = 0;
            int readed;
            var chunkLength = new byte[4];

            while (true)
            {
                readed = _inStream.Read(chunkLength, 0, 4);

                if (readed == 0)
                {
                    _transformQueue.Enqueue(null);
                    _outputQueue.Enqueue(null);
                    break;
                }

                var buffer = new byte[BitConverter.ToInt32(chunkLength, 0)];

                readed = _inStream.Read(buffer, 0, buffer.Length);

                var chunk = new Chunk(chunkId++, buffer, readed);

                if(!_transformQueue.TryEnqueue(chunk) || !_outputQueue.TryEnqueue(chunk))
                    break;
            }

            foreach (var thread in _transformThreads)
            {
                thread.Join();
            }
            _outputThread.Join();
        }

        public void Abort()
        {
            _transformQueue.Quit();
            _outputQueue.Quit();
        }

        #region IDisposable implementation

        public void Dispose()
        {
            _inStream.Close();
            _outStream.Close();
        }

        #endregion

        #endregion

        #region Private methods

        private void CompressThread(object o)
        {
            _startEvent.WaitOne();

            try
            {
                Chunk chunk;

                while (_transformQueue.TryDequeue(out chunk))
                {
                    if (chunk == null)
                    {
                        _transformQueue.Quit();
                        break;
                    }

                    using (var ms = new MemoryStream())
                    {
                        using (var zip = new GZipStream(ms, CompressionMode.Compress, true))
                        {
                            zip.Write(chunk.Data, 0, chunk.Length);
                        }

                        chunk.Data = ms.ToArray();
                        chunk.Length = (int) ms.Length;
                    }

                    chunk.TransformedEvent.Set();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Runtime error!");
                Console.WriteLine(e.Message);

                Abort();
            }
        }

        private void DecompressThread(object o)
        {
            _startEvent.WaitOne();

            try
            {
                Chunk chunk;
                int readed;

                while (_transformQueue.TryDequeue(out chunk))
                {
                    if (chunk == null)
                    {
                        _transformQueue.Quit();
                        break;
                    }

                    var data = new byte[BufferSize];

                    using (var ms = new MemoryStream(chunk.Data))
                    using (var zip = new GZipStream(ms, CompressionMode.Decompress))
                    {
                        readed = zip.Read(data, 0, data.Length);
                    }
                    
                    chunk.Data = data;
                    chunk.Length = readed;

                    chunk.TransformedEvent.Set();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Runtime error!");
                Console.WriteLine(e.Message);

                Abort();
            }
        }

        private void PasteThread(object o)
        {
            try
            {
                var type = o.ToString();

                _startEvent.WaitOne();

                Chunk chunk;

                while (_outputQueue.TryDequeue(out chunk))
                {
                    if (chunk == null)
                    {
                        _outputQueue.Quit();
                        break;
                    }

                    chunk.TransformedEvent.WaitOne();

                    Console.WriteLine("Writing chunk # {0}, size of {1} bytes", chunk.Id, chunk.Length);

                    if (type == "compression")
                        _outStream.Write(BitConverter.GetBytes(chunk.Length), 0, 4);

                    _outStream.Write(chunk.Data, 0, chunk.Length);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Runtime error!");
                Console.WriteLine(e.Message);

                Abort();
            }
        }

        private void InitTransformThreads(ParameterizedThreadStart start)
        {
            for (int i = 0; i < _transformThreads.Length; i++)
            {
                _transformThreads[i] = new Thread(start);
                _transformThreads[i].Start();
            }
        }

        #endregion
    }
}