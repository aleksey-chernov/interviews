﻿using System.Threading;

namespace GZipTestNew
{
    internal class Chunk
    {
        public readonly EventWaitHandle TransformedEvent = new ManualResetEvent(false);
        private volatile byte[] _data;
        private volatile int _length;

        public int Id { get; private set; }

        public byte[] Data
        {
            get { return _data; }
            set { _data = value; }
        }

        public int Length
        {
            get { return _length; }
            set { _length = value; }
        }

        public Chunk(int id, byte[] data, int length)
        {
            Id = id;
            _data = data;
            _length = length;
        }
    }
}