﻿using System.Collections.Generic;
using System.Threading;

namespace GZipTestNew
{
    internal class FixedBlockedQueue<T>
    {
        #region Fields

        private readonly Queue<T> _queue = new Queue<T>();
        private readonly int _maxSize;
        private bool _quit;

        #endregion

        #region Constructors

        public FixedBlockedQueue(int maxSize)
        {
            _maxSize = maxSize;
        }

        #endregion

        #region Data Properties

        public int Count
        {
            get
            {
                lock (_queue)
                {
                    return _queue.Count;
                }
            }
        }

        #endregion

        #region Public methods

        public void Enqueue(T item)
        {
            lock (_queue)
            {
                while (_queue.Count >= _maxSize)
                {
                    Monitor.Wait(_queue);
                }

                _queue.Enqueue(item);
    
                Monitor.PulseAll(_queue);
            }
        }

        public T Dequeue()
        {
            lock (_queue)
            {
                while (_queue.Count == 0)
                {
                    Monitor.Wait(_queue);
                }

                T item = _queue.Dequeue();

                Monitor.PulseAll(_queue);

                return item;
            }
        }

        public bool TryEnqueue(T item)
        {
            lock (_queue)
            {
                while (_queue.Count >= _maxSize)
                {
                    Monitor.Wait(_queue);
                }

                if (_quit)
                    return false;

                _queue.Enqueue(item);

                Monitor.PulseAll(_queue);

                return true;
            }
        }

        public bool TryDequeue(out T obj)
        {
            lock (_queue)
            {
                while (_queue.Count == 0)
                {
                    if (_quit)
                    {
                        obj = default(T);
                        return false;
                    }

                    Monitor.Wait(_queue);
                }

                obj = _queue.Dequeue();

                Monitor.PulseAll(_queue);
                
                return true;
            }
        }

        public void Quit()
        {
            lock (_queue)
            {
                _quit = true;

                Monitor.PulseAll(_queue);
            }
        }

        #endregion
    }
}
