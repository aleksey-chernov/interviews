namespace calculator
{
	extern "C"
	{
		__declspec(dllexport) double doIt(int typeWork, double operandA, double operandB, int& errorCode)
		{
			switch (typeWork)
			{
				case 0:
					return operandA + operandB;
				case 1:
					return operandA - operandB;
				case 2:
					if(operandB == 0)
					{
						errorCode = 1;
						return 0;
					}
					else
					{
						return operandA / operandB;
					}
				case 3:
					return operandA * operandB;
				default:
					errorCode = 2;
					return 0;
			}
		};
	}
}