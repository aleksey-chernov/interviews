﻿using System;
using System.Windows.Data;
using CalculatorApp.Domain;

namespace CalculatorApp.Infrastructure
{
    public sealed class OperationTypeToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var operationType = (OperationType)value;

            switch (operationType)
            {
                case OperationType.Addition:
                    return "+";
                case OperationType.Difference:
                    return "-";
                case OperationType.Multiplication:
                    return "*";
                case OperationType.Division:
                    return "/";
                default:
                    throw new FormatException("Bad enum value");
            }
        }
        
        public object ConvertBack(object value, Type targetType,
          object parameter, System.Globalization.CultureInfo culture)
        {
            var operationType = value.ToString();

            switch (operationType)
            {
                case "+":
                    return OperationType.Addition;
                case "-":
                    return OperationType.Difference;
                case "*":
                    return OperationType.Multiplication;
                case "/":
                    return OperationType.Division;
                default:
                    throw new FormatException("Bad enum value");
            }
        }
    }
}
