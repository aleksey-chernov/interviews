﻿using System;
using System.Windows.Data;
using System.Windows.Media;
using CalculatorApp.Domain;

namespace CalculatorApp.Infrastructure
{
   public sealed class ConsoleMessageTypeToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var messageType = (ConsoleMessageType)value;

            switch (messageType)
            {
                case ConsoleMessageType.Error:
                    return Colors.Red;
                case ConsoleMessageType.Request:
                    return Colors.Blue;
                case ConsoleMessageType.Result:
                    return Colors.Green;
                default:
                    throw new FormatException("Bad enum value");
            }
        }

        // No need to implement converting back on a one-way binding 
        public object ConvertBack(object value, Type targetType,
          object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}