﻿namespace CalculatorApp.Infrastructure
{
    public static class ValidationTools
    {
        public static bool IsValidDouble(string number)
        {
            double digit;
            return double.TryParse(number, out digit);
        }

        public static bool IsValidInt(string number)
        {
            int digit;
            return int.TryParse(number, out digit);
        }
    }
}
