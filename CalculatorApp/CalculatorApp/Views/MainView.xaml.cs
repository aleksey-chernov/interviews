﻿using System.Globalization;
using System.Windows;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace CalculatorApp.Views
{
    /// <summary>
    ///     Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : Window, IXmlSerializable
    {
        public MainView()
        {
            InitializeComponent();
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            reader.ReadStartElement();
            Width = reader.ReadElementContentAsDouble("Width", "");
            Height = reader.ReadElementContentAsDouble("Height", "");
            Left = reader.ReadElementContentAsDouble("Left", "");
            Top = reader.ReadElementContentAsDouble("Top", "");
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteElementString("Width", Width.ToString(CultureInfo.InvariantCulture));
            writer.WriteElementString("Height", Height.ToString(CultureInfo.InvariantCulture));
            writer.WriteElementString("Left", Left.ToString(CultureInfo.InvariantCulture));
            writer.WriteElementString("Top", Top.ToString(CultureInfo.InvariantCulture));
        }
    }
}
