﻿using System.IO;
using System.Xml.Serialization;
using CalculatorApp.Views;
using CalculatorApp.ViewModels;
using System.Windows;

namespace CalculatorApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            var xs = new XmlSerializer(typeof(MainView));
            MainView mainView;

            try
            {
                using (var s = File.OpenRead("settings.xml"))
                    mainView = (MainView) xs.Deserialize(s);
            }
            catch
            {
                mainView = new MainView();
            }

            mainView.DataContext = new MainViewModel();
            mainView.ShowDialog();

            using (var s = File.Create("settings.xml"))
                xs.Serialize(s, mainView);
        }
    }
}