﻿using System;
using System.Runtime.InteropServices;

namespace CalculatorApp.Domain
{
    public enum OperationType
    {
        Addition,
        Difference,
        Division,
        Multiplication
    };

    public static class Calculator
    {
        [DllImport("CalculatorLib.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern double doIt(int typeWork, double operandA, double operandB, ref int errorCode);

        public static double Operation(OperationType operation, double operandA, double operandB)
        {
            int errorCode = 0;

            double result = doIt((int) operation, operandA, operandB, ref errorCode);

            switch (errorCode)
            {
                case 1:
                    throw new DivideByZeroException();
                case 2:
                    throw new ApplicationException("Wrong operation type!");
                default:
                    return result;
            }
        }
    }
}