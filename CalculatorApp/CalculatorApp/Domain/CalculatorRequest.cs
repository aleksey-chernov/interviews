﻿using System.Globalization;
using System.Threading;

namespace CalculatorApp.Domain
{
    public class CalculatorRequest
    {
        private readonly double _operandA;
        private readonly double _operandB;
        private readonly OperationType _operation;
        private readonly int _operationTime;

        public CalculatorRequest(double operandA, double operandB, OperationType operation, int operationTime)
        {
            _operandA = operandA;
            _operandB = operandB;
            _operation = operation;
            _operationTime = operationTime;
        }

        public string Process()
        {
            Thread.Sleep(_operationTime);

            return Calculator.Operation(_operation, _operandA, _operandB).ToString(CultureInfo.InvariantCulture);
        }
    }
}