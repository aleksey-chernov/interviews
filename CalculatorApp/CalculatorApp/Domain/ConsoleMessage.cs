﻿namespace CalculatorApp.Domain
{
    public enum ConsoleMessageType
    {
        Result,
        Request,
        Error
    };

    public class ConsoleMessage
    {
        public ConsoleMessage(ConsoleMessageType messageType, string message)
        {
            Message = message;
            MessageType = messageType;
        }

        public string Message { get; private set; }
        public ConsoleMessageType MessageType { get; private set; }
    }
}