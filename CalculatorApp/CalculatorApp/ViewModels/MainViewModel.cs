﻿using System;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using CalculatorApp.Commands;
using CalculatorApp.Domain;
using CalculatorApp.Infrastructure;

namespace CalculatorApp.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged, IDataErrorInfo, IDisposable
    {
        #region Fields

        private ObservableCollection<ConsoleMessage> _consoleMessages;

        private readonly BlockingCollection<CalculatorRequest> _requestQueue =
            new BlockingCollection<CalculatorRequest>();

        #endregion

        #region Constructors

        public MainViewModel()
        {
            Init();

            Task.Factory.StartNew(Calculate);
        }

        #endregion

        #region Command Properties

        private ICommand _addRequestCommand;

        public ICommand AddRequestCommand
        {
            get { return _addRequestCommand ?? (_addRequestCommand = new RelayCommand(param => AddRequest(), param => IsValid)); }
        }

        #endregion

        #region Data Properties

        public ObservableCollection<ConsoleMessage> ConsoleMessages
        {
            get { return _consoleMessages ?? (_consoleMessages = new ObservableCollection<ConsoleMessage>()); }
        }

        public string DisplayName { get; set; }

        public string OperandA { get; set; }

        public string OperandB { get; set; }

        public OperationType Operation { get; set; }

        public string OperationTime { get; set; }

        public int RequestQueueCount
        {
            get { return _requestQueue.Count; }
        }

        #endregion

        #region Private Methods

        private void Init()
        {
            DisplayName = "Калькулятор";
            Operation = OperationType.Addition;
            OperandA = OperandB = OperationTime = "0";
        }

        private void AddRequest()
        {
            var converter = new OperationTypeToStringConverter();

            _requestQueue.Add(new CalculatorRequest(double.Parse(OperandA), double.Parse(OperandB), Operation, int.Parse(OperationTime)));
            _consoleMessages.Insert(0,
                new ConsoleMessage(ConsoleMessageType.Request,
                    string.Format("Добавлен запрос на вычисление: {0} {1} {2}", OperandA,
                        converter.Convert(Operation, Enum.GetUnderlyingType(Operation.GetType()), null, null), OperandB)));
            OnPropertyChanged("RequestQueueCount");
        }

        private void Calculate()
        {
            foreach (CalculatorRequest request in _requestQueue.GetConsumingEnumerable())
            {
                OnPropertyChanged("RequestQueueCount");

                try
                {
                    string result = request.Process();

                    Application.Current.Dispatcher.Invoke(() => _consoleMessages.Insert(0,
                        new ConsoleMessage(ConsoleMessageType.Result,
                            "Запрос на вычисление обработан, результат: " + result)));
                }
                catch (Exception ex)
                {
                    string message;

                    if (ex is DivideByZeroException)
                    {
                        message = "Ошибка вычисления: деление на ноль";
                    }
                    else
                    {
                        message = "Ошибка при обработке запроса на вычисление: " + ex.Message;
                    }

                    Application.Current.Dispatcher.Invoke(
                        () => _consoleMessages.Insert(0, new ConsoleMessage(ConsoleMessageType.Error, message)));
                }
            }
        }

        #endregion

        #region Validation

        public bool IsValid
        {
            get
            {
                return new[] { "OperandA", "OperandB", "OperationTime" }.All(prop => GetValidationError(prop) == null);
            }
        }

        #region IDataErrorInfo Members

        public string Error
        {
            get { return null; }
        }

        public string this[string columnName]
        {
            get
            {
                var error = GetValidationError(columnName);

                CommandManager.InvalidateRequerySuggested();

                return error;
            }
        }

        private string GetValidationError(string propertyName)
        {
            string error = null;

            switch (propertyName)
            {
                case "OperandA":
                    if (!ValidationTools.IsValidDouble(OperandA) || string.IsNullOrEmpty(OperandA))
                    {
                        error = "Неправильное значение операнда А";
                    }
                    break;
                case "OperandB":
                    if (!ValidationTools.IsValidDouble(OperandB) || string.IsNullOrEmpty(OperandB))
                    {
                        error = "Неправильное значение операнда Б";
                    }
                    break;
                case "OperationTime":
                    if (!ValidationTools.IsValidInt(OperationTime) || string.IsNullOrEmpty(OperationTime))
                    {
                        error = "Неправильное значение времени вычисления";
                    }
                    break;
            }

            return error;
        }

        #endregion

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                PropertyChanged(this, e);
            }
        }

        #endregion

        #region IDispose Members

        public void Dispose()
        {
            _requestQueue.CompleteAdding();
        }

        #endregion
    }
}