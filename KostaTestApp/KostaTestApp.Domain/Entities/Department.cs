﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KostaTestApp.Domain.Entities
{
    public class Department
    {
        public int Id { get; set; }

        public int? ParentId { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public IEnumerable<Department> Departments { get; set; }
    }
}