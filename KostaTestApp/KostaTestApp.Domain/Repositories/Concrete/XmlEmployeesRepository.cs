﻿using KostaTestApp.Domain.Entities;
using KostaTestApp.Domain.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KostaTestApp.Domain.Repositories.Concrete
{
    public class XmlEmployeesRepository : XmlBaseRepository, IEmployeesRepository
    {
        public IEnumerable<Employee> GetAll()
        {
            var result = from x in Document.Root.Elements(Namespace + "tblEmps")
                         select new Employee
                         {
                             Id = (int)x.Element(Namespace + "nEmpId"),
                             DepartmentId = (int)x.Element(Namespace + "nDepId"),
                             FirstName = (string)x.Element(Namespace + "cFirstName"),
                             LastName = (string)x.Element(Namespace + "cLastName"),
                             PatronymicName = (string)x.Element(Namespace + "cPatronymic")
                         };

            return result;
        }

        public IEnumerable<Employee> GetByDepartmentId(int departmentId)
        {
            var result = from x in Document.Root.Elements(Namespace + "tblEmps")
                         let depId = (int)x.Element(Namespace + "nDepId")
                         where depId == departmentId
                         select new Employee
                         {
                             Id = (int)x.Element(Namespace + "nEmpId"),
                             DepartmentId = depId,
                             FirstName = (string)x.Element(Namespace + "cFirstName"),
                             LastName = (string)x.Element(Namespace + "cLastName"),
                             PatronymicName = (string)x.Element(Namespace + "cPatronymic")
                         };

            return result;
        }
    }
}