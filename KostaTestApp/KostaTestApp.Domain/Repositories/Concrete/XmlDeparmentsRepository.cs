﻿using KostaTestApp.Domain.Entities;
using KostaTestApp.Domain.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KostaTestApp.Domain.Repositories.Concrete
{
    public class XmlDepartmentsRepository : XmlBaseRepository, IDepartmentsRepository
    {
        public IEnumerable<Department> GetAllAsTree()
        {
            return GetDepartmentsRecursive(null);
        }

        private IEnumerable<Department> GetDepartmentsRecursive(int? parentId)
        {
            var result = from x in Document.Root.Elements(Namespace + "tblDeps")
                         let id = (int)x.Element(Namespace + "nDepId")
                         let pId = (int?)x.Element(Namespace + "nPrnDepId")
                         where pId == parentId
                         select new Department
                         {
                             Id = id,
                             ParentId = pId,
                             Code = (string)x.Element(Namespace + "cCode"),
                             Name = (string)x.Element(Namespace + "cName"),
                             Departments = GetDepartmentsRecursive(id)
                         };

            return result;
        }
    }
}