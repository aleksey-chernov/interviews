﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace KostaTestApp.Domain.Repositories.Concrete
{
    public abstract class XmlBaseRepository
    {
        protected readonly XDocument Document;
        protected readonly XNamespace Namespace;

        public XmlBaseRepository()
        {
            Document = XDocument.Load("Firm.xml");
            Namespace = Document.Root.Name.Namespace;
        }
    }
}