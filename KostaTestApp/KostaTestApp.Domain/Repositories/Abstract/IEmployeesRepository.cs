﻿using KostaTestApp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KostaTestApp.Domain.Repositories.Abstract
{
    public interface IEmployeesRepository
    {
        IEnumerable<Employee> GetAll();

        IEnumerable<Employee> GetByDepartmentId(int departmentId);
    }
}