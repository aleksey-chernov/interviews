﻿using KostaTestApp.Domain.Entities;
using KostaTestApp.Presenters;
using KostaTestApp.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KostaTestApp
{
    public partial class MainForm : Form, IDepartmentsView, IEmployeesView
    {
        private readonly DepartmentsPresenter _departmentsPresenter;
        private readonly EmployeesPresenter _employeesPresenter;

        public MainForm()
        {
            InitializeComponent();

            Load += MainForm_Load;
            treeViewDepartments.AfterSelect += treeViewDepartments_AfterSelect;

            _departmentsPresenter = new DepartmentsPresenter(this);
            _employeesPresenter = new EmployeesPresenter(this);
        }

        void treeViewDepartments_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Tag != null)
            {
                _employeesPresenter.Display((int)e.Node.Tag);
            }
        }

        void MainForm_Load(object sender, EventArgs e)
        {
            _departmentsPresenter.Display();
        }
        
        public IEnumerable<Department> Departments
        {
            set
            {
                treeViewDepartments.Nodes.Clear();

                AddDepartmentsRecursive(null, value);
            }
        }

        public IEnumerable<Employee> Employees
        {
            set 
            {
                listViewEmployees.Items.Clear();

                foreach (var emp in value)
                {
                    var item = new ListViewItem(emp.LastName);
                    item.SubItems.Add(emp.FirstName);
                    item.SubItems.Add(emp.PatronymicName);
                    listViewEmployees.Items.Add(item);
                }
            }
        }

        private void AddDepartmentsRecursive(TreeNode parent, IEnumerable<Department> departments)
        {
            foreach (var dep in departments)
            {
                var node = new TreeNode
                {
                    Text = dep.Name,
                    ToolTipText = dep.Code,
                    Tag = dep.Id
                };

                if (parent == null)
                {
                    treeViewDepartments.Nodes.Add(node);
                }
                else
                {
                    parent.Nodes.Add(node);
                }

                AddDepartmentsRecursive(node, dep.Departments);
            }
        }
    }
}