﻿using KostaTestApp.Domain.Repositories.Abstract;
using KostaTestApp.Domain.Repositories.Concrete;
using KostaTestApp.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KostaTestApp.Presenters
{
    public class DepartmentsPresenter
    {
        private readonly IDepartmentsView _departmentsView;
        private readonly IDepartmentsRepository _departmentsRepo;

        public DepartmentsPresenter(IDepartmentsView departmentsView)
        {
            _departmentsView = departmentsView;
            _departmentsRepo = new XmlDepartmentsRepository();
        }

        public void Display()
        {
            _departmentsView.Departments = _departmentsRepo.GetAllAsTree();
        }
    }
}