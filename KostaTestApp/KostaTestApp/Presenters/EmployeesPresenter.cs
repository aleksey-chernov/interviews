﻿using KostaTestApp.Domain.Repositories.Abstract;
using KostaTestApp.Domain.Repositories.Concrete;
using KostaTestApp.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KostaTestApp.Presenters
{
    public class EmployeesPresenter
    {
        private readonly IEmployeesView _employeesView;
        private readonly IEmployeesRepository _employeesRepo;

        public EmployeesPresenter(IEmployeesView employeesView)
        {
            _employeesView = employeesView;
            _employeesRepo = new XmlEmployeesRepository();
        }

        public void Display()
        {
            _employeesView.Employees = _employeesRepo.GetAll();
        }

        public void Display(int departmentId)
        {
            _employeesView.Employees = _employeesRepo.GetByDepartmentId(departmentId);
        }
    }
}