﻿using KostaTestApp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KostaTestApp.Views
{
    public interface IDepartmentsView
    {
        IEnumerable<Department> Departments { set; }
    }
}