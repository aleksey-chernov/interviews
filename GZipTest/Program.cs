﻿using System;
using System.IO;

namespace GZipTest
{
    internal class Program
    {
        private static FileInfo _inFileInfo;
        private static FileInfo _outFileInfo;
        private static GZipArchiver _archiver;

        private static int Main(string[] args)
        {
            Console.CancelKeyPress += Console_CancelKeyPress;

            if (args.Length != 3)
            {
                Console.WriteLine(@"Usage: [compress 'FileName' 'ArchiveName'] | [decompress 'ArchiveName' 'FileName']");
                return 0;
            }

            try
            {
                _inFileInfo = new FileInfo(args[1]);
                _outFileInfo = new FileInfo(args[2]);

                if (_outFileInfo.Exists)
                    throw new ArgumentException(@"File with name {0} already exists. Please, enter another file name.",
                        args[2]);

                //_archiver = new GZipArchiver(_inFileInfo.OpenRead(), _outFileInfo.OpenWrite());

                switch (args[0])
                {
                    case "compress":
                        for (int i = 0; i < 10; i++)
                        {
                            using (var archiver = new GZipArchiver(_inFileInfo.OpenRead(), _outFileInfo.OpenWrite()))
                            {
                                archiver.Compress();
                            }
                            _outFileInfo.Delete();
                        }
                        break;
                    case "decompress":
                        //_archiver.Decompress();
                        break;
                    default:
                        throw new ArgumentException("Unknown command!");
                }

                Console.ReadKey();
                return 1;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return 0;
            }
            finally
            {
                if (_archiver != null)
                    _archiver.Dispose();
            }
        }

        static void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            if (_archiver != null)
            {
                _archiver.Abort();
                _archiver.Dispose();
            }

            try
            {
                _outFileInfo.Delete();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}