﻿namespace GZipTest
{
    class Chunk
    {
        public int Id { get; private set; }
        public byte[] Data { get; private set; }
        public int Length { get; private set; }

        public Chunk(int id, byte[] data, int length)
        {
            Id = id;
            Data = data;
            Length = length;
        }
    }
}