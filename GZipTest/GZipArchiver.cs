﻿using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Threading;
using GZipTest;

namespace GZipTest
{
    public class GZipArchiver : IDisposable
    {
        #region Fields

        private readonly Stream _inStream, _outStream;

        private const int BufferSize = 64 * 1024;

        private readonly Queue _queue = Queue.Synchronized(new Queue());

        private readonly EventWaitHandle _startEvent = new ManualResetEvent(false);
        private readonly EventWaitHandle _readEvent = new ManualResetEvent(true);
        private readonly EventWaitHandle[] _writeEvents = new EventWaitHandle[Environment.ProcessorCount];

        private readonly object _streamLock = new object();
        private readonly object _sequenceLock = new object();

        private volatile bool _endOfQueue;
        private volatile bool _abort;
        private volatile int _currChunkId;

        private readonly Stopwatch _watch = new Stopwatch();

        #endregion

        #region Constructors

        public GZipArchiver(Stream inStream, Stream outStream) 
        {
            if (inStream == null)
                throw new ArgumentNullException("inStream");
            if (outStream == null)
                throw new ArgumentNullException("outStream");

            _inStream = inStream;
            _outStream = outStream;

            for (int i = 0; i < Environment.ProcessorCount; i++)
            {
                _writeEvents[i] = new ManualResetEvent(true);
            }
        }

        #endregion

        #region Public Methods

        public void Compress()
        {
            _currChunkId = 0;

            var threads = new Thread[Environment.ProcessorCount];

            for (int i = 0; i < threads.Length; i++)
            {
                threads[i] = new Thread(CompressThread);

                threads[i].Start(i);
            }

            int chunkId = 0;
            int readed = 0;
                
            _watch.Start();

            _startEvent.Set();
            while (true)
            {
                var buffer = new byte[BufferSize];

                lock (_streamLock)
                {
                    readed = _inStream.Read(buffer, 0, buffer.Length);
                }

                if (readed == 0)
                {
                    lock(_queue.SyncRoot)
                        _queue.Enqueue(null);
                    break;
                }

                _readEvent.WaitOne();
                lock (_queue.SyncRoot)
                    _queue.Enqueue(new Chunk(chunkId++, buffer, readed));
                _readEvent.Reset();

                if (_abort)
                    break;
            }

            if (!_abort)
            {
                foreach (Thread thread in threads)
                {
                    thread.Join();
                }

                _watch.Stop();

                Console.WriteLine("Compression finished!");
                Console.WriteLine("Elapsed time: {0} ms", _watch.ElapsedMilliseconds);
            }

            _watch.Stop();
            _watch.Reset();
        }

        public void Decompress()
        {
            _currChunkId = 0;

            var threads = new Thread[Environment.ProcessorCount];

            for (int i = 0; i < threads.Length; i++)
            {
                threads[i] = new Thread(DecompressThread);

                threads[i].Start(i);
            }

            int chunkId = 0;
            int readed = 0;
            var chunkLength = new byte[4];

            _watch.Start();

            _startEvent.Set();
            while (true)
            {
                lock (_streamLock)
                {
                    readed = _inStream.Read(chunkLength, 0, 4);
                }

                if (readed == 0)
                {
                    _queue.Enqueue(null);
                    break;
                }

                var buffer = new byte[BitConverter.ToInt32(chunkLength, 0)];
                lock(_streamLock)
                {
                    readed = _inStream.Read(buffer, 0, buffer.Length);
                }
                
                _readEvent.WaitOne();
                _queue.Enqueue(new Chunk(chunkId++, buffer, readed));
                _readEvent.Reset();

                if (_abort)
                    break;
            }

            if (!_abort)
            {
                foreach (var thread in threads)
                {
                    thread.Join();
                }

                _watch.Stop();

                Console.WriteLine("Decompression finished!");
                Console.WriteLine("Elapsed time: {0} ms", _watch.ElapsedMilliseconds);
            }

            _watch.Stop();
            _watch.Reset();
        }

        public void Abort()
        {
            _endOfQueue = true;
            _abort = true;

            foreach (var wait in _writeEvents)
            {
                wait.Set();
            }

            _readEvent.Set();
        }

        #region IDisposable implementation

        public void Dispose()
        {
            _inStream.Close();
            _outStream.Close();
        }

        #endregion

        #endregion

        #region Private methods

        private void CompressThread(object o)
        {
            try
            {
                int threadId = (int)o;

                _startEvent.WaitOne();

                while (!_endOfQueue)
                {
                    Chunk chunk = null;

                    lock (_queue.SyncRoot)
                    {
                        if (_queue.Count > 0)
                        {
                            chunk = _queue.Dequeue() as Chunk;
                            if (chunk == null)
                            {
                                _endOfQueue = true;
                                return;
                            }
                        }
                    }

                    _readEvent.Set();

                    if (chunk != null)
                    {
                        byte[] data;

                        using (var ms = new MemoryStream())
                        {
                            using (var zip = new GZipStream(ms, CompressionMode.Compress, true))
                            {
                                zip.Write(chunk.Data, 0, chunk.Length);
                            }

                            data = ms.ToArray();
                        }

                        while (true)
                        {
                            _writeEvents[threadId].WaitOne();

                            if (_abort)
                                break;

                            lock (_sequenceLock)
                            {
                                if (chunk.Id != _currChunkId)
                                {
                                    _writeEvents[threadId].Reset();
                                    continue;
                                }

                                //Console.WriteLine("Writing chunk # {0}, size of {1} bytes", chunk.Id, data.Length);

                                lock (_streamLock)
                                {
                                    _outStream.Write(BitConverter.GetBytes(data.Length), 0, 4);
                                    _outStream.Write(data, 0, data.Length);
                                }

                                _currChunkId++;

                                foreach (var wait in _writeEvents)
                                {
                                    wait.Set();
                                }

                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Runtime error!");
                Console.WriteLine(e.Message);

                Abort();
            }
        }

        private void DecompressThread(object o)
        {
            try
            {
                int threadId = (int)o;

                _startEvent.WaitOne();

                int readed = 0;
                var data = new byte[BufferSize];

                while (!_endOfQueue)
                {
                    Chunk chunk = null;

                    lock (_queue.SyncRoot)
                    {
                        if (_queue.Count > 0)
                        {
                            chunk = _queue.Dequeue() as Chunk;
                            if (chunk == null)
                            {
                                _endOfQueue = true;
                                return;
                            }
                        }
                    }

                    _readEvent.Set();

                    if (chunk != null)
                    {
                        using (var ms = new MemoryStream(chunk.Data))
                        {
                            using (var zip = new GZipStream(ms, CompressionMode.Decompress))
                            {
                                readed = zip.Read(data, 0, data.Length);
                            }
                        }

                        while (true)
                        {
                            _writeEvents[threadId].WaitOne();

                            if (_abort)
                                return;

                            lock (_sequenceLock)
                            {
                                if (chunk.Id != _currChunkId)
                                {
                                    _writeEvents[threadId].Reset();
                                    continue;
                                }

                                Console.WriteLine("Writing chunk # {0}, size of {1} bytes", chunk.Id, readed);

                                lock (_streamLock)
                                {
                                    _outStream.Write(data, 0, readed);
                                }

                                _currChunkId++;

                                foreach (var wait in _writeEvents)
                                {
                                    wait.Set();
                                }

                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Runtime error!");
                Console.WriteLine(e.Message);

                Abort();
            }
        }

        #endregion
    }
}